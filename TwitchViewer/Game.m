//
//  Game.m
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "Game.h"
#import "Channel.h"


@implementation Game

@dynamic channels;
@dynamic logo;
@dynamic logo_url;
@dynamic name;
@dynamic viewers;
@dynamic game_id;
@dynamic channel;

@end
