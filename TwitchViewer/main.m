//
//  main.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
