//
//  DataLoader.h
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Game;

typedef void(^SearchBlock)(BOOL success);

@interface DataLoader : NSObject

@property (nonatomic, readonly) NSMutableArray *topGamesResult;
@property (nonatomic, readonly) NSMutableArray *channelsResult;

- (void)loadedTopGamesWithCompletion:(SearchBlock)block;
- (void)loadedChannelsWithOffset:(NSInteger)offset
       andWithRelationToGame:(Game *)game
              withCompletion:(SearchBlock)block;

@end
