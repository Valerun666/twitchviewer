//
//  BackgroundLabel.m
//  TwitchViewer
//
//  Created by Valerun on 09.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "BackgroundLabel.h"

@implementation BackgroundLabel

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.text = @"No data is currently available. Please pull down to refresh";
        self.textColor = [UIColor colorWithRed:100.0/255.0
                                                         green:65.0/255.0
                                                          blue:165.0/255.0
                                                         alpha:1];
        self.font = [UIFont systemFontOfSize:25];
        self.textAlignment = NSTextAlignmentCenter;
        self.numberOfLines = 0;
    }
    
    return self;
}

@end
