//
//  LoadingDataCell.m
//  TwitchViewer
//
//  Created by Valerun on 19.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "LoadingDataCell.h"

@implementation LoadingDataCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        self.activityIndicator.color = [UIColor colorWithRed:100.0/255.0
                                                       green:65.0/255.0
                                                        blue:165.0/255.0
                                                       alpha:1];
        
        [self.contentView addSubview:self.activityIndicator];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.activityIndicator.center = self.contentView.center;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
