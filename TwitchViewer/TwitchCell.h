//
//  GameCell.h
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwitchCell : UITableViewCell

@property (nonatomic) UIImageView *logoView;
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UILabel *firstLabel;
@property (nonatomic) UILabel *firstDataLabel;
@property (nonatomic) UILabel *secondDataLabel;
@property (nonatomic) UILabel *secondLabel;

@end
