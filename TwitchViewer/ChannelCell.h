//
//  ChannelCell.h
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChannelCell : UITableViewCell

@property (nonatomic) UIImageView *logoView;
@property (nonatomic) UILabel *nameLabel;
@property (nonatomic) UILabel *numberOfViewers;
@property (nonatomic) UILabel *viewersLabel;

@end
