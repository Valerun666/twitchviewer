//
//  ChannelDataProvider.m
//  TwitchViewer
//
//  Created by Valerun on 14.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "ChannelDataProvider.h"
#import "CoreDataController.h"

@implementation ChannelDataProvider

- (NSFetchedResultsController *)channelFetchedResultsController {
    if (_channelFetchedResultsController != nil) {
        return _channelFetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Channel" inManagedObjectContext:[[CoreDataController sharedInstance] managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_at" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"game.game_id == %@", _gameId];
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setFetchBatchSize:25];
    
    _channelFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[CoreDataController sharedInstance] managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    
    return _channelFetchedResultsController;
}

- (NSArray *)channelFetched {
    NSError *error;
    if (![self.channelFetchedResultsController performFetch:&error]) {
        NSLog(@"FetchResultsController failed while fetch");
        return nil;
    } else {
        return [[[self.channelFetchedResultsController sections] objectAtIndex:0] objects];
    }
}

@end
