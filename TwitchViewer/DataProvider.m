//
//  DataProvider.m
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "DataProvider.h"

@implementation DataProvider

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.games = [GameDataProvider new];
    }
    
    return self;
}

- (ChannelDataProvider *)channels {
    return [ChannelDataProvider new];
}

@end
