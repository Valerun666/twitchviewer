//
//  TopGamesTableViewController.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "TopGamesTableViewController.h"

#import "AppDelegate.h"
#import "ChannelsTableViewController.h"
#import "CoreDataController.h"
#import "Game.h"
#import "BackgroundView.h"
#import "TwitchCell.h"
#import "DataProvider.h"

@interface TopGamesTableViewController () <NSFetchedResultsControllerDelegate>

@end

@implementation TopGamesTableViewController {
    NSManagedObjectContext *context;
    UIRefreshControl *refreshControl;
    NSArray *gamesArray;
    AppDelegate *appDelegate;
    LoadingStatus loadingStatus;
    BackgroundView *backgroundView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Top Games";
    
    loadingStatus = DataIsLoading;
    
    backgroundView = [[BackgroundView alloc] initWithFrame:self.view.bounds];
    self.tableView.backgroundView = backgroundView;
    
    [self updateBackgroundViewAccordingToLoadingStatus:loadingStatus];
    
    appDelegate = [UIApplication sharedApplication].delegate;
    
    gamesArray = [[[appDelegate dataProvider] games] gameFetched];
    
    [self.tableView registerClass:[TwitchCell class] forCellReuseIdentifier:@"GameCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadedTopGamesNotification:)
                                                 name:kLoadedTopGames
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNetworkErrorWhileLoadingGames:)
                                                 name:kNetworkErrorWhileLoadGames
                                               object:nil];
    
    refreshControl = [UIRefreshControl new];
    refreshControl.backgroundColor = [UIColor colorWithRed:100.0/255.0
                                                     green:65.0/255.0
                                                      blue:165.0/255.0
                                                     alpha:1];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[CoreDataController sharedInstance] saveData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadedTopGamesNotification:(NSNotification *)notification {
    [self updateViewAccordingToLoadingStatus:DataLoaded];
}

- (void)handleNetworkErrorWhileLoadingGames:(NSNotification *)notification
{
    [self updateViewAccordingToLoadingStatus:LoadingFailed];
}

- (void)updateViewAccordingToLoadingStatus:(LoadingStatus)status {
    loadingStatus = status;
    
    if (!self.refreshControl) {
        self.refreshControl = refreshControl;
    }
    
    gamesArray = [[[appDelegate dataProvider] games] gameFetched];

    [self reloadData];
}

- (void)updateBackgroundViewAccordingToLoadingStatus:(LoadingStatus)status {
    if (status == DataIsLoading) {
        [self hideTableViewSeparator];
        [backgroundView.activityIndicator startAnimating];
        [backgroundView.activityIndicator setHidden:NO];
        [backgroundView.messageLabel setHidden:YES];
    } else if (status == DataLoaded) {
        [self showTableViewSeparator];
        [backgroundView.activityIndicator stopAnimating];
        [backgroundView.activityIndicator setHidden:YES];
    } else if (LoadingFailed && !gamesArray.count) {
        [self hideTableViewSeparator];
        [backgroundView.activityIndicator stopAnimating];
        [backgroundView.activityIndicator setHidden:YES];
        [backgroundView.messageLabel setHidden:NO];
    } else {
        [self showTableViewSeparator];
        [backgroundView.activityIndicator stopAnimating];
        [backgroundView.activityIndicator setHidden:YES];
        [backgroundView.messageLabel setHidden:YES];
    }
}

- (void)hideTableViewSeparator {
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)showTableViewSeparator {
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self.tableView.separatorColor = [UIColor grayColor];
}

- (void)refreshData {
    if (loadingStatus != DataIsLoading) {
        [[CoreDataController sharedInstance] loadGames];
    } else {
        [refreshControl endRefreshing];
    }
}

- (void)reloadData {
    [self updateBackgroundViewAccordingToLoadingStatus:loadingStatus];
    
    [self.tableView reloadData];
    
    if (refreshControl.isRefreshing && loadingStatus != DataIsLoading) {
        NSString *title = @"Loading...";
        
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        
        refreshControl.attributedTitle = attributedTitle;
        
        [refreshControl endRefreshing];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LoadedTopGames" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NetworkError" object:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (loadingStatus == DataIsLoading) {
        return 0;
    } else {
        return [gamesArray count];
    }
}

- (void)configureCell:(TwitchCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Game *topGame = gamesArray[indexPath.row];
    
    cell.nameLabel.text = topGame.name;
    cell.secondDataLabel.text = [NSString stringWithFormat:@"%@", topGame.channels];
    cell.secondLabel.text = @"Channels:";
    cell.firstDataLabel.text = [NSString stringWithFormat:@"%@", topGame.viewers];
    cell.firstLabel.text = @"Viewers:";
    [cell.logoView setImage:[UIImage imageWithData:topGame.logo]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TwitchCell *cell = (TwitchCell *)[tableView dequeueReusableCellWithIdentifier:@"GameCell"];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Game *selectedGame = gamesArray[indexPath.row];
    
    ChannelsTableViewController *channelsController = [ChannelsTableViewController new];
    channelsController.game = selectedGame;
    
    [self.navigationController pushViewController:channelsController animated:YES];
}

@end
