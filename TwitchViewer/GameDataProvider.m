//
//  GameDataProvider.m
//  TwitchViewer
//
//  Created by Valerun on 14.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "GameDataProvider.h"
#import "CoreDataController.h"

@implementation GameDataProvider

- (NSFetchedResultsController *)gameFetchedResultsController {
    if (_gameFetchedResultsController != nil) {
        return _gameFetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Game" inManagedObjectContext:[[CoreDataController sharedInstance] managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"viewers" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    [fetchRequest setFetchBatchSize:20];
    
    _gameFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[CoreDataController sharedInstance] managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    
    return _gameFetchedResultsController;
}

- (NSArray *)gameFetched {
    NSError *error;
    if (![self.gameFetchedResultsController performFetch:&error]) {
        NSLog(@"FetchResultsController failed while fetch");
        return nil;
    } else {
        return [[[self.gameFetchedResultsController sections] objectAtIndex:0] objects];
    }
}


@end
