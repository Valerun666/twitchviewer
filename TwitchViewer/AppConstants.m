//
//  AppConstants.m
//  TwitchViewer
//
//  Created by Valerun on 13.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "AppConstants.h"

NSString * const kLoadedTopGames                = @"LoadedTopGames";
NSString * const kNetworkErrorWhileLoadGames    = @"NetworkErrorWhileLoadGames";
NSString * const kLoadedChannels                = @"LoadedChannels";
NSString * const kNetworkErrorWhileLoadChannels = @"NetworkErrorWhileLoadChannels";
