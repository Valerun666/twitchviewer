//
//  ChannelsTableViewController.h
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"

@interface ChannelsTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) Game *game;

@end
