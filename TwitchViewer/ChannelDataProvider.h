//
//  ChannelDataProvider.h
//  TwitchViewer
//
//  Created by Valerun on 14.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelDataProvider : NSObject

@property (nonatomic) NSNumber *gameId;

@property (nonatomic) NSArray *channelFetched;
@property (nonatomic) NSFetchedResultsController *channelFetchedResultsController;

@end
