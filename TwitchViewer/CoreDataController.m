//
//  CoreDataController.m
//  TwitchViewer
//
//  Created by Valerun on 03.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "CoreDataController.h"

#import "Channel.h"
#import "DataLoader.h"
#import "Game.h"

@implementation CoreDataController

+ (id)sharedInstance {
    static CoreDataController *sharedInstance = nil;
    static dispatch_once_t onceTocken;
    
    dispatch_once(&onceTocken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - Insert data into base

- (void)loadGames {
    DataLoader *gamesLoader = [DataLoader new];
    
    [gamesLoader loadedTopGamesWithCompletion:^(BOOL success) {
        if (success) {
            //firstly remove all games
            [self deleteAllObjects:@"Game" withRelationToObject:nil withName:nil];
            
            dispatch_queue_t request_queue = dispatch_queue_create("com.twitchViewer.loadGames", NULL);
            dispatch_async(request_queue, ^{
                NSManagedObjectContext *backgroundMoc = [self backgroundManagedObjectContext];
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(mergeChangesInGames:)
                                                             name:NSManagedObjectContextDidSaveNotification
                                                           object:backgroundMoc];
                
                
                NSArray *loadedGames = [gamesLoader topGamesResult];
                
                for (NSDictionary *dict in loadedGames) {
                    Game *topGame = [NSEntityDescription insertNewObjectForEntityForName:@"Game" inManagedObjectContext:backgroundMoc];
                    
                    topGame.game_id     = dict[@"game_id"];
                    topGame.name        = dict[@"name"];
                    topGame.logo_url    = dict[@"logo_url"];
                    topGame.viewers     =  dict[@"viewers"];
                    topGame.channels    = dict[@"channels"];
                    topGame.logo        = dict[@"logo"];

                }
                
                NSError *error;
                BOOL success = [backgroundMoc save:&error];
                if (!success) {
                    NSLog(@"Failed while saving Parse Channels");
                }
            });
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkErrorWhileLoadGames object:self];
        }
    }];
}

- (void)mergeChangesInGames:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self managedObjectContext] mergeChangesFromContextDidSaveNotification:notification];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadedTopGames object:self];
    });
}

- (void)loadChannelsWithOffset:(NSInteger)offset andRelation:(Game *)game withRefresh:(BOOL)refresh{
    DataLoader *channelsLoader = [DataLoader new];
    
    [channelsLoader loadedChannelsWithOffset:offset andWithRelationToGame:game withCompletion:^(BOOL success) {
        if (success) {
            
            if (refresh) {
                [self deleteAllObjects:@"Channel" withRelationToObject:@"game.name" withName:game.name];
            }
            
            dispatch_queue_t request_queue = dispatch_queue_create("com.twitchViewer.loadChannels", NULL);
            dispatch_async(request_queue, ^{
                NSArray *loadedChannels = [channelsLoader channelsResult];
                
                NSDictionary *firstObject = loadedChannels[0];
                
                Game *game = firstObject[@"game"];
                
                NSManagedObjectContext *backgroundMoc = [self backgroundManagedObjectContext];
                
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Game" inManagedObjectContext:[self managedObjectContext]];
                
                Game *newGame = (Game *)[backgroundMoc objectWithID:[game objectID]];
                
                NSDictionary *attrs = [game dictionaryWithValuesForKeys:[[entity attributesByName] allKeys]];
                
                [newGame setValuesForKeysWithDictionary:attrs];
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(mergeChangesInChannels:)
                                                             name:NSManagedObjectContextDidSaveNotification
                                                           object:backgroundMoc];
                
                
                
                
                for (NSDictionary *resultDict in loadedChannels) {
                    Channel *channel = [self checkIfExistsinTableEntity:@"Channel" withName:resultDict[@"name"]];
                    
                    if (channel == nil) {
                        channel = [NSEntityDescription insertNewObjectForEntityForName:@"Channel" inManagedObjectContext:backgroundMoc];
                        
                        channel.game        = newGame;
                        channel.created_at  = resultDict[@"created"];
                    } else {
                        channel.game        = game;
                        channel.updated_at  = resultDict[@"created"];
                    }
                    
                    channel.followers   = resultDict[@"followers"];
                    channel.viewers     = resultDict[@"viewers"];
                    channel.name        = resultDict[@"name"];
                    channel.logo_url    = resultDict[@"logo_url"];
                    
                    if (![channel.logo_url isEqualToString:@"NoLogo"]) {
                        channel.logo = [NSData dataWithContentsOfURL:[NSURL URLWithString:channel.logo_url]];
                    }
                }
                
                NSError *error;
                BOOL success = [backgroundMoc save:&error];
                if (!success) {
                    NSLog(@"Failed while saving Parse Channels");
                }
            });
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkErrorWhileLoadChannels object:self];
        }
    }];
}

- (void)mergeChangesInChannels:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self managedObjectContext] mergeChangesFromContextDidSaveNotification:notification];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadedChannels object:self];
    });
}

- (NSManagedObjectContext *)backgroundManagedObjectContext {
    NSManagedObjectContext *backgroundMoc = [NSManagedObjectContext new];
    
    [backgroundMoc setPersistentStoreCoordinator:[self.managedObjectContext persistentStoreCoordinator]];
    [backgroundMoc setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    return backgroundMoc;
}

#pragma mark - Core Data

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistantStoreCoordinator {
    if (_persistantStoreCoordinator != nil) {
        return _persistantStoreCoordinator;
    }
    
    _persistantStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TwitchViewer.sqlite"];
    
    NSLog(@"%@", storeURL);
    
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    if (![_persistantStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"%@ !!! %@", failureReason, [error localizedDescription]);
        abort();
    }
    
    return _persistantStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *persistantStoreCoordinator = [self persistantStoreCoordinator];
    
    if (!persistantStoreCoordinator) {
        return nil;
    }
    
    _managedObjectContext = [NSManagedObjectContext new];
    [_managedObjectContext setPersistentStoreCoordinator:persistantStoreCoordinator];
    
    return _managedObjectContext;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
}

#pragma mark - Aditional methods

- (NSManagedObject *)checkIfExistsinTableEntity:(NSString *)entity withName:(NSString *)name {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"name == %@", name]];
    
    NSError *error;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (result.count > 0) {
        return [result firstObject];
    }
    
    return nil;
}

- (void)deleteAllObjects:(NSString *)entityDescription withRelationToObject:(NSString *)object withName:(NSString *)relationOBjectName  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    if (object != nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", object,
                                  relationOBjectName];
        [fetchRequest setPredicate:predicate];
    }
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in items) {
        [_managedObjectContext deleteObject:managedObject];
    }
    if (![_managedObjectContext save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entity,error);
    }
}

- (void)saveData {
    NSError *error;
    
    if (![[self managedObjectContext] save:&error]) {
        NSLog(@"Saving data failed!");
        abort();
    }
}

@end
