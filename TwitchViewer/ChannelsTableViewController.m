//
//  ChannelsTableViewController.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "ChannelsTableViewController.h"

#import "AppDelegate.h"
#import "Channel.h"
#import "ChannelDataProvider.h"
#import "CoreDataController.h"
#import "DataProvider.h"
#import "LoadEngine.h"
#import "BackgroundView.h"
#import "TwitchCell.h"
#import "UIView+AnimationHidden.h"
#import "LoadingDataCell.h"

@implementation ChannelsTableViewController {
    AppDelegate *appDelegate;
    BackgroundView *backgroundView;
    NSMutableArray *channelsArray;
    ChannelDataProvider *dataProvider;
    UIView *footer;
    UIActivityIndicatorView *footerSpinner;
    LoadingStatus loadingStatus;
    int offset;
    UIRefreshControl *refreshControl;
    BOOL refreshing;
    BOOL uploadingNewPage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Channels";
    
    offset = 25;
    
    backgroundView = [[BackgroundView alloc] initWithFrame:self.view.bounds];
    self.tableView.backgroundView = backgroundView;
    [self updateBackgroundViewAccordingToLoadingStatus:DataIsLoading];
    
    [self.tableView registerClass:[TwitchCell class] forCellReuseIdentifier:@"ChannelCell"];
    [self.tableView registerClass:[LoadingDataCell class] forCellReuseIdentifier:@"LoadingCell"];
    
    
    appDelegate = [UIApplication sharedApplication].delegate;
    dataProvider = [[appDelegate dataProvider] channels];
    dataProvider.gameId = _game.game_id;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadedChannelsNotification:)
                                                 name:kLoadedChannels
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNetworkErrorWhileLoadChannels:)
                                                 name:kNetworkErrorWhileLoadChannels
                                               object:nil];
    
    [self loadData];
    
    refreshControl = [UIRefreshControl new];
    refreshControl.backgroundColor = [UIColor colorWithRed:100.0/255.0
                                                     green:65.0/255.0
                                                      blue:165.0/255.0
                                                     alpha:1];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[CoreDataController sharedInstance] saveData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadData {
    loadingStatus = DataIsLoading;
    
    [[CoreDataController sharedInstance] loadChannelsWithOffset:offset andRelation:_game withRefresh:NO];
}

- (void)loadedChannelsNotification:(NSNotification *)notification {
    [self updateViewAccordingToLoadingStatus:DataLoaded];
}

- (void)handleNetworkErrorWhileLoadChannels:(NSNotification *)notification {
    [self updateViewAccordingToLoadingStatus:LoadingFailed];
}

- (void)updateViewAccordingToLoadingStatus:(LoadingStatus)status {
    loadingStatus = status;
    
    if (!self.refreshControl) {
        self.refreshControl = refreshControl;
    }
    
    channelsArray = [NSMutableArray arrayWithArray:[dataProvider channelFetched]];
    
    [self reloadData];
}

- (void)updateBackgroundViewAccordingToLoadingStatus:(LoadingStatus)status {
    if (status == DataIsLoading) {
        [self hideTableViewSeparator];
        [backgroundView.activityIndicator startAnimating];
        [backgroundView.activityIndicator setHidden:NO];
        [backgroundView.messageLabel setHidden:YES];
    } else if (status == DataLoaded) {
        [self showTableViewSeparator];
        [backgroundView.activityIndicator stopAnimating];
        [backgroundView.activityIndicator setHidden:YES];
    } else if (LoadingFailed && !channelsArray.count) {
        [self hideTableViewSeparator];
        [backgroundView.activityIndicator stopAnimating];
        [backgroundView.activityIndicator setHidden:YES];
        [backgroundView.messageLabel setHidden:NO];
    } else {
        [self showTableViewSeparator];
        [backgroundView.activityIndicator stopAnimating];
        [backgroundView.activityIndicator setHidden:YES];
        [backgroundView.messageLabel setHidden:YES];
    }
}

- (void)hideTableViewSeparator {
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)showTableViewSeparator {
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    self.tableView.separatorColor = [UIColor grayColor];
}

- (void)refreshData {
    if (loadingStatus != DataIsLoading) {
        refreshing = YES;
        
        loadingStatus = DataIsLoading;
        [[CoreDataController sharedInstance] loadChannelsWithOffset:25 andRelation:self.game withRefresh:YES];
    } else {
        [refreshControl endRefreshing];
    }
}

- (void)reloadData {
    [self updateBackgroundViewAccordingToLoadingStatus:loadingStatus];
    
    [self.tableView reloadData];
    
    if (refreshControl.isRefreshing && loadingStatus != DataIsLoading) {
        NSString *title = @"Loading...";
        
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        
        refreshControl.attributedTitle = attributedTitle;
        
        refreshing = NO;
        offset = 25;
        
        [refreshControl endRefreshing];
    }
}

- (void)addLoadingCell {
    uploadingNewPage = YES;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:channelsArray.count inSection:0];
    
    [self.tableView beginUpdates];

    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self.tableView endUpdates];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (loadingStatus == DataIsLoading) {
        return 0;
    } else {
        return channelsArray.count + (uploadingNewPage ? 1 : 0);
    }
}

- (void)configureCell:(TwitchCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Channel *channel = channelsArray[indexPath.row];
    
    cell.nameLabel.text = channel.name;
    
    if ([channel.logo_url isEqualToString:@"NoLogo"]) {
        cell.logoView.image = [UIImage imageNamed:@"NoLogo"];
    } else {
        [cell.logoView setImage:[UIImage imageWithData:channel.logo]];
    }
    
    cell.secondLabel.text = @"Followers:";
    cell.secondDataLabel.text = [NSString stringWithFormat:@"%@", channel.followers];
    cell.firstLabel.text = @"Views:";
    cell.firstDataLabel.text = [NSString stringWithFormat:@"%@", channel.viewers];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TwitchCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ChannelCell"];
    
    if (uploadingNewPage) {
        LoadingDataCell *loadingCell = [LoadingDataCell new];
        [loadingCell.activityIndicator startAnimating];
        
        uploadingNewPage = NO;
        
        [channelsArray removeLastObject];
        
        return loadingCell;
    } else {
        [self configureCell:cell atIndexPath:indexPath];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        if (loadingStatus == DataLoaded && !refreshing) {
            offset += 25;
            
            [self addLoadingCell];
            [self loadData];
        }
    }
}


@end
