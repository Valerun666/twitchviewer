//
//  LoadingDataCell.h
//  TwitchViewer
//
//  Created by Valerun on 19.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingDataCell : UITableViewCell

@property (nonatomic) UIActivityIndicatorView *activityIndicator;

@end
