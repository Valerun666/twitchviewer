//
//  SyncEngine.h
//  TwitchViewer
//
//  Created by Valerun on 06.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncEngine : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
