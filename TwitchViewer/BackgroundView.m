//
//  LoadingDataCell.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "BackgroundView.h"

@implementation BackgroundView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.activityIndicator.center = self.center;
        self.activityIndicator.color = [UIColor colorWithRed:100.0/255.0
                                                       green:65.0/255.0
                                                        blue:165.0/255.0
                                                       alpha:1];
        
        [self addSubview:self.activityIndicator];
        
        self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        self.messageLabel.text = @"No data is currently available. Check internet connection";
        self.messageLabel.textColor = [UIColor colorWithRed:100.0/255.0
                                                 green:65.0/255.0
                                                  blue:165.0/255.0
                                                 alpha:1];
        self.messageLabel.font = [UIFont systemFontOfSize:25];
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.hidden = YES;
        
        [self addSubview:self.messageLabel];
    }
    
    return self;
}

@end
