//
//  LoadingDataCell.h
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackgroundView : UIView

@property (nonatomic) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) UILabel *messageLabel;

@end
