//
//  DataLoader.m
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "DataLoader.h"
#import "Game.h"

@implementation DataLoader {
    NSURL *topGamesURL;
}

- (instancetype)init {
    if ((self = [super init])) {
        topGamesURL = [NSURL URLWithString:@"https://api.twitch.tv/kraken/games/top?limit=10&offset=0"];
        _topGamesResult = [[NSMutableArray alloc] initWithCapacity:10];
        _channelsResult = [NSMutableArray new];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    
    return self;
}

- (void)loadedTopGamesWithCompletion:(SearchBlock)block {
    [self performLoadForURL:topGamesURL urlType:@"game" withRelationToObject:nil andCompletion:^(BOOL success) {
        block(success);
    }];
}

- (void)loadedChannelsWithOffset:(NSInteger)offset
           andWithRelationToGame:(Game *)game
                  withCompletion:(SearchBlock)block{
    NSString *gameName  = [game.name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *stringUrl = [NSString stringWithFormat:@"https://api.twitch.tv/kraken/search/channels?limit=25&offset=%ld&q=%@",(long)offset, gameName];
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    
    [self performLoadForURL:url urlType:@"channel" withRelationToObject:game andCompletion:^(BOOL success) {
        block(success);
    }];
}

- (void)performLoadForURL:(NSURL *)url  urlType:(NSString *)type withRelationToObject:(id)object andCompletion:(SearchBlock)block {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
            [request setTimeoutInterval:120];
        }
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (object != nil) {
            NSMutableDictionary *responseResult = [NSMutableDictionary dictionaryWithDictionary:responseObject];
            [responseResult setObject:object forKey:@"relation"];
            [self parseDictionary:responseResult withObjects:type withCompletionBlock:^(BOOL success) {
                block(success);
            }];
        } else {
            [self parseDictionary:responseObject withObjects:type withCompletionBlock:^(BOOL success) {
                block(success);
            }];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (!operation.isCancelled) {
            block(NO);
        }
    }];
    
    [operation start];
}

- (void)parseDictionary:(NSDictionary *)dictionary withObjects:(NSString *)objectType withCompletionBlock:(SearchBlock)completion {
    if ([objectType isEqualToString:@"game"]) {
        [self parseDictionaryWithGames:dictionary];
        completion(YES);
    } else if ([objectType isEqualToString:@"channel"]) {
        [self parseDictionaryWithChannels:dictionary withCompletionBlock:^(BOOL success) {
            completion(success);
        }];
    }
}

- (void)parseDictionaryWithGames:(NSDictionary *)dictionarry {
    NSArray *games = dictionarry[@"top"];
    
    for (NSDictionary *resultDict in games) {
        NSDictionary *gameInfo = resultDict[@"game"];
        NSDictionary *logoDict = gameInfo[@"logo"];
        
        NSDictionary *topGame = @{@"name" : gameInfo[@"name"],
                                  @"game_id" : [NSNumber numberWithInt:[gameInfo[@"_id"] intValue]],
                                  @"logo_url" : logoDict[@"medium"],
                                  @"viewers" : [NSNumber numberWithInt:[resultDict[@"viewers"] intValue]],
                                  @"channels": [NSNumber numberWithInt:[resultDict[@"channels"] intValue]],
                                  @"logo"    : [NSData dataWithContentsOfURL:[NSURL URLWithString:logoDict[@"medium"]]]
                                  };
        
        [_topGamesResult addObject:topGame];
    }
}

- (void)parseDictionaryWithChannels:(NSDictionary *)dictionary withCompletionBlock:(SearchBlock)comletion{
    NSArray *channels = dictionary[@"channels"];
    
    Game *newGame = dictionary[@"relation"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (NSDictionary *resultDict in channels) {
                    NSString *logoURL = resultDict[@"logo"];
            
                    if (logoURL == [NSNull null]) {
                        logoURL = @"NoLogo";
                    }
            
                    NSDictionary *channel = @{@"name"       : resultDict[@"name"],
                                              @"game"       : newGame,
                                              @"followers"  : resultDict[@"followers"],
                                              @"viewers"    : resultDict[@"views"],
                                              @"logo_url"   : logoURL,
                                              @"created"    : [NSDate date]};
                    
                    [_channelsResult addObject:channel];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            comletion(YES);
        });
    });
}

@end
