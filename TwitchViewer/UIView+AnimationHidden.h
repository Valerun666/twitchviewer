//
//  UIView+AnimationHidden.h
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AnimationHidden)

- (void)setHiddenAnimation:(BOOL)hide;

@end
