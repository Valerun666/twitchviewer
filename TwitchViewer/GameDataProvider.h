//
//  GameDataProvider.h
//  TwitchViewer
//
//  Created by Valerun on 14.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameDataProvider : NSObject

@property (nonatomic) NSArray *gameFetched;
@property (nonatomic) NSFetchedResultsController *gameFetchedResultsController;

@end
