//
//  Search.h
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SearchBlock)(BOOL success);

@interface LoadEngine : NSObject

@property (nonatomic, readonly) NSMutableArray *topGamesResult;
@property (nonatomic, readonly) NSMutableArray *channelsResult;
@property (nonatomic) NSNumber *channelsQuantity;
@property (nonatomic) NSURL *topGamesURL;
@property (nonatomic) NSManagedObjectContext *context;


- (void)performSearchForURL:(NSURL *)url  urlType:(NSString *)type withRelationToObject:(id)object andCompletion:(SearchBlock)block;

@end
