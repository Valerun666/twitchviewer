//
//  Channel.m
//  TwitchViewer
//
//  Created by Valerun on 13.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "Channel.h"
#import "Game.h"


@implementation Channel

@dynamic created_at;
@dynamic updated_at;
@dynamic followers;
@dynamic logo;
@dynamic logo_url;
@dynamic name;
@dynamic viewers;
@dynamic game;

@end
