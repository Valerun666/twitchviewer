//
//  GameCell.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "TwitchCell.h"

@implementation TwitchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.secondLabel = [UILabel new];
        self.secondLabel.textColor = [UIColor lightGrayColor];
        self.secondLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.secondLabel];
        
        self.logoView = [UIImageView new];
        [self.contentView addSubview:self.logoView];
        
        self.nameLabel = [UILabel new];
        self.nameLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.nameLabel];
        
        self.secondDataLabel = [UILabel new];
        self.secondDataLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.secondDataLabel];
        
        self.firstDataLabel = [UILabel new];
        self.firstDataLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.firstDataLabel];
        
        self.firstLabel = [UILabel new];
        self.firstLabel.textColor = [UIColor lightGrayColor];
        self.firstLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.firstLabel];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.logoView.frame = CGRectMake(5,
                                     0,
                                     60,
                                     60);
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.logoView.frame) + 10,
                                      10,
                                      240,
                                      20);
    
    self.firstLabel.frame = CGRectMake(CGRectGetMaxX(self.logoView.frame) + 10,
                                         CGRectGetMaxY(self.nameLabel.frame) + 5,
                                         50,
                                         15);
    
    self.firstDataLabel.frame = CGRectMake(CGRectGetMaxX(self.firstLabel.frame) + 10,
                                            CGRectGetMaxY(self.nameLabel.frame) + 5,
                                            70,
                                            15);
    
    self.secondDataLabel.frame = CGRectMake(CGRectGetMaxX(self.bounds) - 60,
                                            CGRectGetMaxY(self.nameLabel.frame) + 5,
                                            50,
                                            15);
    
    self.secondLabel.frame = CGRectMake(CGRectGetMinX(self.secondDataLabel.frame) - 60,
                                         CGRectGetMaxY(self.nameLabel.frame) + 5,
                                         60,
                                         15);
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
