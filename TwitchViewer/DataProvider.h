//
//  DataProvider.h
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameDataProvider.h"
#import "ChannelDataProvider.h"

@interface DataProvider : NSObject

@property (nonatomic) GameDataProvider *games;
@property (nonatomic) ChannelDataProvider *channels;

@end
