//
//  Search.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "LoadEngine.h"
#import "Game.h"
#import "Channel.h"
#import <AFNetworkReachabilityManager.h>
#import "CoreDataController.h"

static NSOperationQueue *queue = nil;

@interface LoadEngine()

@end

@implementation LoadEngine

- (instancetype)init {
    if ((self = [super init])) {
        _topGamesURL = [NSURL URLWithString:@"https://api.twitch.tv/kraken/games/top?limit=10&offset=0"];
        _topGamesResult = [[NSMutableArray alloc] initWithCapacity:10];
        _channelsResult = [NSMutableArray new];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        _context = [[CoreDataController sharedInstance] managedObjectContext];
    }
    
    return self;
}

- (void)performSearchForURL:(NSURL *)url urlType:(NSString *)type withRelationToObject:(id)object andCompletion:(SearchBlock)block {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
            [request setTimeoutInterval:120];
        }
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([type isEqualToString:@"game"]) {
            [self parseDictionaryWithGames:responseObject];
        } else {
            _channelsQuantity = responseObject[@"_total"];
            NSLog(@"%@", _channelsQuantity);
            [self parseDictionaryWithChannels:responseObject andRelatedGame:object];
        }
        
        block(YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (!operation.isCancelled) {
            block(NO);
        }
    }];
    
    [operation start];
}

- (void)parseDictionaryWithGames:(NSDictionary *)dictionarry {
    NSArray *games = dictionarry[@"top"];
    
    for (NSDictionary *resultDict in games) {
//        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Game" inManagedObjectContext:_context];
//        
//        Game *topGame = [[Game alloc] initWithEntity:entity insertIntoManagedObjectContext:_context];
        
        Game *topGame = [NSEntityDescription insertNewObjectForEntityForName:@"Game" inManagedObjectContext:_context];
        
        NSDictionary *gameInfo = resultDict[@"game"];
        NSDictionary *logoDict = gameInfo[@"logo"];
        
        topGame.name = gameInfo[@"name"];
        topGame.logo_url = logoDict[@"medium"];
        topGame.viewers =  [NSNumber numberWithInt:[resultDict[@"viewers"] intValue]];
        topGame.channels = [NSNumber numberWithInt:[resultDict[@"channels"] intValue]];
        topGame.logo = [NSData dataWithContentsOfURL:[NSURL URLWithString:topGame.logo_url]];
    }
}

- (void)parseDictionaryWithChannels:(NSDictionary *)dictionary andRelatedGame:(Game *)game{
    NSArray *channels = dictionary[@"channels"];
    
    dispatch_queue_t request_queue = dispatch_queue_create("com.twitchViewer.parseDictionary", NULL);
    dispatch_async(request_queue, ^{
        NSManagedObjectContext *newMoc = [[CoreDataController sharedInstance] backgroundManagedObjectContext];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Game" inManagedObjectContext:[[CoreDataController sharedInstance] managedObjectContext]];
        
        Game *newGame = (Game *)[newMoc objectWithID:[game objectID]];
        
        NSDictionary *attrs = [game dictionaryWithValuesForKeys:[[entity attributesByName] allKeys]];
        
        [newGame setValuesForKeysWithDictionary:attrs];
        
        [[NSNotificationCenter defaultCenter] addObserver:[CoreDataController sharedInstance]
                                                 selector:@selector(mergeChanges:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:newMoc];
        
        for (NSDictionary *resultDict in channels) {
            Channel *channel = [[CoreDataController sharedInstance] checkIfExistsinTableEntity:@"Channel" withName:resultDict[@"name"]];
            
            if (channel == nil) {
                channel = [NSEntityDescription insertNewObjectForEntityForName:@"Channel" inManagedObjectContext:newMoc];
                
                channel.game = newGame;
            } else {
                channel.game = game;
            }
            
            channel.followers = resultDict[@"followers"];
            channel.viewers = resultDict[@"views"];
            channel.name = resultDict[@"name"];
            
            NSString *logo = resultDict[@"logo"];
            
//            if (logo == [NSNull null]) {
//                logo = @"NoLogo";
//                channel.logo_url = logo;
//            } else {
//                channel.logo_url = logo;
//                channel.logo = [NSData dataWithContentsOfURL:[NSURL URLWithString:logo]];
//            }
//            
//            if (channel.data_created != nil) {
//                channel.data_updated = [NSDate date];
//            } else {
//                channel.data_created = [NSDate date];
//            }
        }
        
        NSError *error;
        BOOL success = [newMoc save:&error];
        if (!success) {
            NSLog(@"Failed while saving Parse Channels");
        }
    });
}

@end
