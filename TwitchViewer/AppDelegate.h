//
//  AppDelegate.h
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataProvider;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic) DataProvider *dataProvider;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, readonly) BOOL reachable;

@end

