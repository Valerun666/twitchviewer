//
//  AppDelegate.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "AppDelegate.h"
#import "TopGamesTableViewController.h"
#import "CoreDataController.h"
#import "DataProvider.h"

@interface AppDelegate ()

@end

@implementation AppDelegate {
    NSArray *gameStore;
    BOOL isLoaded;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.dataProvider = [DataProvider new];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    TopGamesTableViewController *topGamesController = [TopGamesTableViewController new];
    [self initialLoad];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:topGamesController];
    
    self.window.rootViewController = navController;
    
    [self.window makeKeyAndVisible];
    
    [self customizeAppearance];
        
    return YES;
}

- (BOOL)reachable {
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

- (void)initialLoad {
    [[CoreDataController sharedInstance] loadGames];
}

- (void)customizeAppearance {
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:100.0/255.0
                                                               green:65.0/255.0
                                                                blue:165.0/255.0
                                                               alpha:1]];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[CoreDataController sharedInstance] saveData];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
