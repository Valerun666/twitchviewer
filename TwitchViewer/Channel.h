//
//  Channel.h
//  TwitchViewer
//
//  Created by Valerun on 13.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Game;

@interface Channel : NSManagedObject

@property (nonatomic, retain) NSDate * created_at;
@property (nonatomic, retain) NSDate * updated_at;
@property (nonatomic, retain) NSNumber * followers;
@property (nonatomic, retain) NSData * logo;
@property (nonatomic, retain) NSString * logo_url;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * viewers;
@property (nonatomic, retain) Game *game;

@end
