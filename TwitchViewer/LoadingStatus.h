//
//  LoadingStatus.h
//  TwitchViewer
//
//  Created by Valerun on 10.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#ifndef TwitchViewer_LoadingStatus_h
#define TwitchViewer_LoadingStatus_h

typedef NS_ENUM(NSInteger, LoadingStatus) {
    DataIsLoading,
    LoadingFailed,
    DataLoaded
};


#endif
