//
//  Game.h
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Channel;

@interface Game : NSManagedObject

@property (nonatomic, retain) NSNumber * channels;
@property (nonatomic, retain) NSData * logo;
@property (nonatomic, retain) NSString * logo_url;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * viewers;
@property (nonatomic, retain) NSNumber * game_id;
@property (nonatomic, retain) NSSet *channel;
@end

@interface Game (CoreDataGeneratedAccessors)

- (void)addChannelObject:(Channel *)value;
- (void)removeChannelObject:(Channel *)value;
- (void)addChannel:(NSSet *)values;
- (void)removeChannel:(NSSet *)values;

@end
