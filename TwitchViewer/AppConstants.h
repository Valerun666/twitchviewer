//
//  AppConstants.h
//  TwitchViewer
//
//  Created by Valerun on 13.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

extern NSString * const kLoadedTopGames;
extern NSString * const kNetworkErrorWhileLoadGames;
extern NSString * const kLoadedChannels;
extern NSString * const kNetworkErrorWhileLoadChannels;
