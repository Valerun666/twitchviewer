//
//  ChannelCell.m
//  TwitchViewer
//
//  Created by Valerun on 18.06.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "ChannelCell.h"

@implementation ChannelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.logoView = [UIImageView new];
        [self.contentView addSubview:self.logoView];
        
        self.nameLabel = [UILabel new];
        self.nameLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:self.nameLabel];
        
        self.numberOfViewers = [UILabel new];
        self.numberOfViewers.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.numberOfViewers];
        
        self.viewersLabel = [UILabel new];
        self.viewersLabel.text = @"Viewers:";
        self.viewersLabel.textColor = [UIColor lightGrayColor];
        self.viewersLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.viewersLabel];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.logoView.frame = CGRectMake(5,
                                     0,
                                     60,
                                     60);
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.logoView.frame) + 10,
                                      10,
                                      240,
                                      20);
    
    self.viewersLabel.frame = CGRectMake(CGRectGetMaxX(self.logoView.frame) + 10,
                                         CGRectGetMaxY(self.nameLabel.frame) + 5,
                                         50,
                                         15);
    
    self.numberOfViewers.frame = CGRectMake(CGRectGetMaxX(self.viewersLabel.frame) + 10,
                                            CGRectGetMaxY(self.nameLabel.frame) + 5,
                                            50,
                                            15);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
