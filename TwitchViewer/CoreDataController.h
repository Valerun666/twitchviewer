//
//  CoreDataController.h
//  TwitchViewer
//
//  Created by Valerun on 03.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Game;

@interface CoreDataController : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistantStoreCoordinator;

+ (id)sharedInstance;

- (NSURL *)applicationDocumentsDirectory;

- (NSManagedObject *)checkIfExistsinTableEntity:(NSString *)entity withName:(NSString *)name;//Checks if exists object in table, if yes - returns it, if no - inserts new object and
//returns it.

- (void)saveData;

- (void)deleteAllObjects:(NSString *)entityDescription
    withRelationToObject:(NSString *)object
                withName:(NSString *)relationOBjectName;

- (NSManagedObjectContext *)backgroundManagedObjectContext;

//Loaders in base
- (void)loadGames;
- (void)loadChannelsWithOffset:(NSInteger)offset andRelation:(Game *)game withRefresh:(BOOL)refresh;

@end
