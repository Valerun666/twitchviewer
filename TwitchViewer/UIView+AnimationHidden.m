//
//  UIView+AnimationHidden.m
//  TwitchViewer
//
//  Created by Valerun on 12.08.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "UIView+AnimationHidden.h"

@implementation UIView (AnimationHidden)

- (void)setHiddenAnimation:(BOOL)hide {
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         if (hide) {
                             self.alpha = 0;
                         } else {
                             self.hidden = NO;
                             self.alpha = 1;
                         }
                     } completion:^(BOOL finished) {
                         if (hide) {
                             self.hidden = YES;
                         }
                     }];
}


@end
